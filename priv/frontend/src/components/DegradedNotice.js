import React from 'react'
import PropTypes from 'prop-types'

import './DegradedNotice.css'

export default function DegradedNotice({ services }) {
  const keys = Object.keys(services)
  const serviceNames = keys.join(', ')

  const plural = keys.length === 1 ? '' : 's'
  const indicative = keys.length === 1 ? 'is' : 'are'

  return (
    <div className="degraded-notice">
      <header>
        {keys.length} service
        {plural} {indicative} unreachable
      </header>
      <p>
        elstat is having trouble contacting <strong>{serviceNames}</strong>.
      </p>
    </div>
  )
}

DegradedNotice.propTypes = {
  services: PropTypes.object.isRequired,
}

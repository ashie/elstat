import hashlib

from .errors import ApiError


def auth_route(handler):
    """Declare an authenticated route."""
    async def _handler(request, *args, **kwargs):
        try:
            pwhash = request.headers['Authorization']
        except KeyError:
            raise ApiError('no password provided', 403)

        correct = request.app.cfg.PASSWORD
        hashed = hashlib.sha256(correct.encode()).hexdigest()

        if pwhash != hashed:
            raise ApiError('invalid password', 403)

        return await handler(request, *args, **kwargs)

    return _handler

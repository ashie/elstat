# Elstat

Status page software.

## Installing

```
git clone https://gitlab.com/elixire/elstat
cd elstat
python3.6 -m pip install -Ur requirements.txt

# edit config.py as you wish
cp config.example.py config.py

# build frontend
cd priv/frontend
# check instructions on README.md
```

## Run

```
python3.6 run.py
```
